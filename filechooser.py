'''
Created on Feb 12, 2014

@author: Daniel

Goes through each repo, picks up to 100 random files, parses the comments out of the files, outputs in JSON format
'''
import os, random, json, subprocess

def splitall(path):     #split directory path into its components
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

maindir = "D:\\github_repos\\"

subdirs = os.walk(maindir).next()[1]    #get subdirectories (repos names)
print subdirs
allfiles = []

for subdir in subdirs:
    directory = maindir + subdir        #full path of subdirectory
    print "Going through " + directory
    files = []
    for dirname, dirnames, filenames in os.walk(directory):     #walks through each subdirectory
        for filename in filenames:
            if filename.endswith(('.scala','.java','.c','.py','.cpp','.cc','.h','.go','.js','.css','.r','.cs','.php','.pl','.perl')):   #grab only source code files
                files.append(os.path.join(dirname, filename))
                #print os.path.join(dirname, filename)
    allfiles.append([directory, files])     #allfiles contains repo directory and a list of all its source code files

awkdirectory = "C:\\Users\\Daniel\\Desktop\\xscc.awk.txt"       #awk script that parses out comments
#json_file = "D:\\repositories.json"                             #grabbed from rails server
outputfile = "D:\\testfile.txt"                                 #temp file generated from awk script
finaloutput = "D:\\sourceccodecomments.json"                    #final output file

#with open(json_file, "r") as f:
#    json_repo = json.load(f)

i=0
for eachfile in allfiles:
    randsample = random.sample(set(eachfile[1]), min(100, len(eachfile[1])))    #select random files from list
    print "Random sample of " + eachfile[0]
    print randsample

    for samplefile in randsample:
        print "=============================PARSING THROUGH " + samplefile + " ======================================"
        command = "gawk -f " + awkdirectory + " extract=comment prune=copyright " + samplefile + " > " + outputfile     #command line to parse out comments
        os.system(command)
        comments = []
        subcomments = []
        output = open(outputfile, "r+")
        firstline = output.readline()
        if (firstline != ''):       #check to see if there are any comments in the file
            prevlinenum = int(firstline.split(' ',1)[0])
            subcomments.append(prevlinenum)
            subcomments.append(firstline.split(' ',1)[1])
            #print "First linenum = " + str(prevlinenum)
            for line in output:
                linenum = int(line.split(' ',1)[0])
                comment = line.split(' ',1)[1]
                if (linenum == (prevlinenum + 1) or (linenum == prevlinenum)):    #same comment
                    subcomments.append(comment)
                elif (linenum != (prevlinenum + 1)):  #new comment
                    subcomments.append(prevlinenum)
                    comments.append(subcomments)    #append the previous block of comments to the main comment array
                    subcomments = []
                    subcomments.append(linenum)
                    subcomments.append(comment)
                else:
                    print "Error"
                prevlinenum = linenum
        output.close()
        splitdir = splitall(samplefile)
        filedir = '\\'.join(map(str,splitdir[2:]))
        finaloutputwriter = open(finaloutput, "a")
        os.chdir(eachfile[0])
        for comment in comments:
            command2 = "git blame -l -L " + str(comment[0]) + "," + str(comment[-1]) + " --before='2013-10-10' " + samplefile   #git blame command line
            output = subprocess.Popen(command2, shell=True, stdout=subprocess.PIPE).communicate()[0] #send command2 to shell, get output
            if (len(output) < 5):
                print "Comment was made after MSR"
            else:
                i+=1
                sha = output.split(" ",1)[0]    #grabs the SHA commit code for the comment's commit (before 10/10/13)
                newdict = {"id":i, "file":filedir, "line_begin":comment[0], "line_end":comment[-1], "comment":" ".join(comment[1:-1]),
                           "commit_sha":sha }
                jsondump = json.dumps(newdict, sort_keys = True)
                print jsondump
                finaloutputwriter.write(jsondump+"\n")
        finaloutputwriter.close()

print "===========================DONE============================"