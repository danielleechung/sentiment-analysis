'''
Created on Feb 13, 2014

@author: Daniel

Clean up source code JSON to remove random useless comments
'''

import ast

samplefile = r"D:\sourcecodecomments.json"
outputfile = r"D:\source_code_comments.json"

sample = open(samplefile,"r")
lines = sample.readlines()
sample.close()

output = open(outputfile,"w")
for line in lines:
    #line2 = json.load(line)
    tempdict = ast.literal_eval(line)
    #print tempdict
    if (tempdict['comment'] == "//\n \n"):  #tempdict.['comment'].isspace() for "\n" comments
        print "Whitespace"
    else:
        output.write(line)
output.close()