'''
Created on Feb 18, 2014

@author: Daniel

Associate users with repositories
'''
import psycopg2 as sql

db = sql.connect("user=sentimental_github password=sentimental_github_2014 dbname=sentimental_github_development host=jarvis.cs.ucdavis.edu port=5432")
#db = sql.connect("user=postgres dbname=sentimental_github_development")
cursor = db.cursor()
"""
    cursor.execute("insert into source_code_comments (comment, commit_sha, file, line_begin, line_end) VALUES (%s, %s, %s, %s, %s)", (comment, commit_sha, filename, line_begin, line_end))
"""
#colnames = [desc[0] for desc in cursor.description]
#print colnames

issue_repo = {}
user_repo = {}
repo = {}

query = "select id,full_name from repositories"
print query
cursor.execute(query)
data = cursor.fetchall()
for datum in data:
    repo[datum[0]] = datum[1]

query2 = "select id, user_id,repo_id from issues"
print query2
cursor.execute(query2)
data = cursor.fetchall()
for datum in data:
    if (not user_repo.get(str(datum[1]))):          #if doesn't exist yet
        user_repo[str(datum[1])] = [datum[2]]
    if (datum[2] not in user_repo[str(datum[1])]):  #if it's not already associated with the user
        user_repo[str(datum[1])].append(datum[2])
    issue_repo[str(datum[0])] = datum[2]
    #print datum
#print issue_repo

query3 = "select issue_comments.user_id, issues.repo_id from issue_comments, issues where issue_comments.issue_id = issues.id"
print query3
cursor.execute(query3)
data = cursor.fetchall()
for datum in data:
    if (not user_repo.get(str(datum[0]))):          #if user doesn't exist yet
        user_repo[str(datum[0])] = [datum[1]]
    if (datum[1] not in user_repo[str(datum[0])]):  #if it's not already associated with the user
        user_repo[str(datum[0])].append(datum[1])
    #print (datum[0],issue_repo[str(datum[1])])

query4 = "select author_id, repo_id from commits"
print query4
cursor.execute(query4)
data = cursor.fetchall()
for datum in data:
    if (not user_repo.get(str(datum[0])) and datum[1] != 0):          #if user doesn't exist yet
        user_repo[str(datum[0])] = [datum[1]]
        if (datum[1] not in user_repo[str(datum[0])]):  #if it's not already associated with the user
            user_repo[str(datum[0])].append(datum[1])

query5 = "select commit_comments.user_id, commits.repo_id from commit_comments,commits where commit_comments.commit_id = commits.id"
print query5
cursor.execute(query5)
data = cursor.fetchall()
for datum in data:
    if (not user_repo.get(str(datum[0])) and datum[1] != 0):          #if user doesn't exist yet
        user_repo[str(datum[0])] = [datum[1]]
        if (datum[1] not in user_repo[str(datum[0])]):  #if it's not already associated with the user
            user_repo[str(datum[0])].append(datum[1])

query6 = "select commits.author_id, commits.repo_id from commits,source_code_comments where commits.sha = source_code_comments.commit_sha"
print query6
cursor.execute(query6)
data = cursor.fetchall()
for datum in data:
    if (not user_repo.get(str(datum[0])) and datum[1] != 0):          #if user doesn't exist yet
        user_repo[str(datum[0])] = [datum[1]]
        if (datum[1] not in user_repo[str(datum[0])]):  #if it's not already associated with the user
            user_repo[str(datum[0])].append(datum[1])

#print user_repo
query7 = "delete from repository_users"
#query7 = "insert into source_code_comments (comment, commit_sha, file, line_begin, line_end) VALUES (%s, %s, %s, %s, %s)"
query8 = "select * from repository_users"

print query7
print query8
cursor.execute(query7)
cursor.execute(query8)
data = cursor.fetchall()
print data
colnames = [desc[0] for desc in cursor.description]
print colnames

i=0
for userrepos in user_repo.items():
    for repository in userrepos[1]:
        print i,[userrepos[0],repository],len(user_repo)
        i+=1
        if (userrepos[0] != 'None'):
            user_id = int(userrepos[0])
        repo_id = repository
        if (user_id != 'None' and repo_id != 'None'):
            cursor.execute("insert into repository_users (user_id, repo_id) VALUES (%s, %s)", (user_id, repo_id))
        #print userrepos[0], repo[repository]



db.commit()
db.close()
print "---DONE---"