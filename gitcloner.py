'''
Created on Feb 11, 2014

@author: Daniel

Clones the Github repositories to DIR_NAME
'''
import git
from git import *
import shutil
import json


json_file = "D:\\repositories.json"

with open(json_file, "r") as f:
    data = json.load(f)
git_urls = []
names = []
for i in range(len(data)):
    git_urls.append(data[i]["git_url"])
    names.append(data[i]["name"])
print git_urls
print names

DIR_NAME = "D:\\github_repos\\"

if os.path.isdir(DIR_NAME):
    shutil.rmtree(DIR_NAME)

os.mkdir(DIR_NAME)

for i in range(len(git_urls)):
    j = i
    newdir = DIR_NAME + names[j] + "\\"
    print "Cloning " + names[j] + " to " + newdir
    print "Repo " + str(j)
    os.mkdir(newdir)
    Repo.clone_from(git_urls[j], newdir)


print "---------------DONE-----------------"


