'''
Created on Feb 18, 2014

@author: Daniel

Insert source code comments into the SQL database

(need to pg_dump after)
'''
import psycopg2 as sql
import ast

beginfile = open("D:\\tempoutput.json")
lines = beginfile.readlines()

db = sql.connect("user=postgres dbname=sentimental_github_development")
cursor = db.cursor()

for line in lines:
    tempdict = ast.literal_eval(line)
    comment = tempdict["comment"]
    commit_sha = tempdict["commit_sha"]
    filename = tempdict["file"]
    line_begin = tempdict["line_begin"]
    line_end = tempdict["line_end"]
    cursor.execute("insert into source_code_comments (comment, commit_sha, file, line_begin, line_end) VALUES (%s, %s, %s, %s, %s)", (comment, commit_sha, filename, line_begin, line_end))

cursor.execute("select * from source_code_comments limit 100")
#colnames = [desc[0] for desc in cursor.description]
#print colnames
data = cursor.fetchall()
for datum in data:
    print datum
db.commit()
db.close()
beginfile.close()
print "---DONE---"